module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: ['@nuxtjs', 'prettier', 'prettier/vue', 'plugin:prettier/recommended', 'plugin:nuxt/recommended'],
  plugins: ['prettier'],
  // add your custom rules here
  rules: {
    'nuxt/no-cjs-in-config': 'off',
    // allow async-await
    'generator-star-spacing': 'off',
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'space-before-function-paren': 'off',
    indent: ['warn', 2],
    semi: [1, 'always'],
    'no-undef': 'warn',
    'no-unused-vars': 'warn',
    'no-trailing-spaces': 'off',
    'spaced-comment': 'off',
    'eol-last': 'off',
    'vue/no-multi-spaces': [
      'warn',
      {
        ignoreProperties: true
      }
    ],
    'vue/html-closing-bracket-newline': [
      'warn',
      {
        singleline: 'never',
        multiline: 'never'
      }
    ],
    'vue/html-closing-bracket-spacing': [
      'warn',
      {
        startTag: 'never',
        endTag: 'never',
        selfClosingTag: 'always'
      }
    ],
    'vue/multiline-html-element-content-newline': [
      'warn',
      {
        ignores: [
          'pre',
          'textarea',
          'a',
          'b-form-checkbox',
          'el-yesno-input',
          'b-row',
          'b-col',
          'b-form-select',
          'b-modal',
          'i',
          'b-dropdown-item',
          'b-button',
          'span',
          'small',
          'datetime'
        ]
      }
    ],
    'vue/html-indent': [
      'warn',
      2,
      {
        attribute: 1,
        closeBracket: 0
      }
    ],
    quotes: ['warn', 'single']
  }
};
